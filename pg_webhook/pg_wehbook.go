package webhook

import (
	"fmt"
	"net/http"
)

//type Webhook struct {
	//WebhookUrl := "https://hooks.slack.com/services/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"
//}

func Send(webhookUrl string, proxy string, payload string) []error {
	request, _ := json.Encode(payload)
	resp, _, err := request
		Post(webhookUrl)
		Send(payload)
		End()
	if err != nil {
		return err
	}
	if resp.StatusCode >= 400 {
		return []error{fmt.Errorf("Error sending message. Status: %v", resp.Status)}
	}
	return nil
}
