API for retrieving information from .igc files
Made by: Marius Håkonsen, marhaako@stud.ntnu.no


Usage:



	GET url/igcinfo/api:

	response type: application/json
	response body:
		{
			"uptime": <uptime of service, ISO8601 format>
			"info": <info>
			"version": <current version>
		}



	POST url/igcinfo/api/igc:

	response type: application/json
	request body:
		{
			"url": "<url to igc file>"
		}
	response body:
		{
			"id": "<generated id>"
		}



	GET url/igcinfo/api/igc:

	response type: application/json
	response body:
		[
			<id1>,
			<id2>,
			<id3>,
			...
		]
	if empty, returns empty array



	GET url/igcinfo/api/igc/<id>

	response type: application/json
	response body:
		{
			"H_date": <date from File Header, H-record>,
			"pilot": <pilot>,
			"glider": <glider>,
			"glider_id": <glider_id>,
			"track_length": <calculated total track length>
		}

	GET url/igcinfo/api/igc/<id>/<field>

	response type: text/plain
	PS: Remember first letter in <field> has to be Upper Case!
	response:
		<Pilot> for Pilot
		<Glider> for Glider
		<Glider_id> for Glider_id
		<Calculated track length> for Track_length
		<H_date> for H-date
