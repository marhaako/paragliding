
package main

import (
	"os"
	"github.com/p3lim/iso8601"	//Format time after iso8601 format
	"fmt"
	"net/http"
	"github.com/marni/goigc"
	"encoding/json"
	"time"			//Time class for uptime
	"strings"
	"reflect"		//Field name in struct
	"strconv"		//Atoi
	"paragliding/pg_webhook"
//	"gopkg.in/mgo.v2"
//	"gopkg.in/mgo.v2/bson"
)
	var lastID int
	var Urls map[int]string
	var startTime time.Time
	var timestamps map[int]string
	var Ids []int
	var t_latest string
	var tickerCap int
type MongoDB struct {
	DatabaseUrl string
	DatabaseName string
	CollectionName string
}

type MetaInfo struct {
	Uptime string `json:"uptime"`
	Info string `json:"info"`
	Version string `json:"version"`
}

type TrackInfo struct {
	H_date string
	Pilot string `json:"pilot"`
	Glider string `json:"glider"`
	Glider_id string `json:"glider_id"`
	Track_length float64 `json:"track_length"`
	Track_src_url string `json:"track_src_url"`
}

type Ticker struct {
	T_latest string `json:"t_latest"`
	T_start string `json:"t_start"`
	T_stop string `json:"t_stop"`
	Tracks []int `json:"tracks"`
	Processing string `json:"processing"`
}

func handleApi(w http.ResponseWriter, r *http.Request) {
	http.Header.Add(w.Header(), "Content-Type", "application/json")		//Set header
	fmt.Println("Handle function for Meta Data")				//Explanatory message
	dura := time.Since(startTime)			//Set uptime
	meta := MetaInfo{Uptime: iso8601.Format(dura), Info: "Service for IGC tracks.", Version: "v1"}  //Sets struct values
	json.NewEncoder(w).Encode(meta)			//Encode the metadata
}

func handleIgc(w http.ResponseWriter, r *http.Request) {
        http.Header.Add(w.Header(), "Content-Type", "application/json")		//Set header
	fmt.Println("Handle function for IGC")

	if (r.Method == http.MethodGet) {	//Get function that returns array of Ids
		Ids = Ids[:0]			//Clear array
		for key := range Urls {		//Add all Urls ids to array
			Ids = append(Ids, key)
		}
		json.NewEncoder(w).Encode(Ids)	//Send json response

	} else if (r.Method == http.MethodPost) {
		var temp map[string]interface{}	//Temporary map for decoding into

		err := json.NewDecoder(r.Body).Decode(&temp)  //Decode request
		if err != nil {			//Checks for decode error
			http.Error(w, http.StatusText(400), 400)
			return
		}				//Check if URL contains a valid .igc
		_, err = igc.ParseLocation(temp["url"].(string))
		if err != nil {			//Error message if not valid
			http.Error(w, http.StatusText(400), 400)
			return
		}
		lastID += 1			//If everything OK, create new url with new id
		Urls[lastID] = temp["url"].(string)
		response := map[string]int{"id": lastID}
		t_latest = time.Now()
		timestamps[lastID] = t_latest
		json.NewEncoder(w).Encode(response) //Respond with the new id
	}

}

func handleID(w http.ResponseWriter, r *http.Request) {
	http.Header.Add(w.Header(), "Content-Type", "application/json")  //Set header
	fmt.Println("Handle function for given <id>")

	u := strings.Split(r.URL.Path, "/")	//Splits url into string parts
	id, err := strconv.Atoi(u[4])   //Converts id to int
        if err != nil {                 //Error message if failed
                http.Error(w, http.StatusText(400), 400)
		return
        }
	if (len(u) > 6) {		//If length of url exceeds 6
		http.Error(w, http.StatusText(404), 404)
		return
	}
	if _, ok := Urls[id]; ok {			//Checks if url with given ID exists
		s := Urls[id]
		track, err := igc.ParseLocation(s)	//Parse the track
		if err != nil {				//If cant parse, error
			http.Error(w, http.StatusText(404), 404)
		}

		t := TrackInfo{track.Date.String(), track.Pilot, track.GliderType, track.GliderID, track.Points[0].Distance(track.Points[len(track.Points)-1]), s}
		if (len(u) == 6) {					//If length of url is 6
			http.Header.Add(w.Header(), "Content-Type", "text/plain")
			field := t.getField(u[5])		//Gets the field
			if field != "" {				//If fieldname exists
				json.NewEncoder(w).Encode(field)	//Encode value
				return
			} else {					//Error message,fieldname not found
				http.Error(w, http.StatusText(404), 400)
				return
			}
		}
                json.NewEncoder(w).Encode(t)

	} else if !ok {					//If Url does not exist, error
		http.Error(w, http.StatusText(404), 404)
	}


}

func handleRoot(w http.ResponseWriter, r *http.Request) {  //Error for accessing root
	Send("https://hooks.slack.com/services/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", "", "This is just a test")
	//http.Redirect(w, r, "/paragliding/api", 301)
}

func handleError(w http.ResponseWriter, r *http.Request) {
	http.Error(w, http.StatusText(404), 404)
}

func handleTicker(w http.ResponseWriter, r *http.Request) {
	u := strings.Split(r.URL.Path, "/")     //Splits url into string parts
	if (len(u) == 5) {
		if (u[4] == "latest") {
			http.Header.Add(w.Header(), "Content-Type", "text/plain")
			json.NewEncoder(w).Encode(t_latest)
			return
		}

	}
	else if (len(u) == 4) {
	ids [tickerCap] int
	for key := tickerCap {         //Add all Urls ids to array
                Ids = append(Ids, key)
        }
		t := Ticker{t_latest, 
	}
}

func getPort() string {			//Setting up port for heroku
	var port = os.Getenv("PORT")
	if (port == "") {
		port = "8080"
	fmt.Println("No PORT variable detected, default " + port)
	}
	return (":" + port)
}

func (t TrackInfo) getField(field string) interface{} {		//Function for returning field value with given name
	r := reflect.ValueOf(&t).Elem()
	for i := 0; i < r.NumField(); i++ {		//Search through name of fields, return value if match
		if r.Type().Field(i).Name == field {
			if field == "Track_length" {
				return r.Field(i).Interface().(float64)
			}
			return r.Field(i).String()
		}
	}
	return ""	//If not found, return ""
}


func main() {
	lastID = 0	//Zero out lastID
	startTime = time.Now()		//Start clock
	Urls = make(map[int]string)
	Ids = make([]int, len(Urls))
	tickerCap = 5
	http.HandleFunc("/paragliding", handleRoot)
	http.HandleFunc("/paragliding/", handleError)
	http.HandleFunc("/paragliding/api", handleApi)
	http.HandleFunc("/paragliding/api/igc", handleIgc)
	http.HandleFunc("/paragliding/api/igc/", handleID)
	http.HandleFunc("/paragliding/api/ticker", handleTicker)
	//err := http.ListenAndServe(getPort(), nil)
	err := http.ListenAndServe("localhost:8080", nil)
	if err != nil {
		fmt.Println(err)
	}
}


